# <img src="https://cdn.rawgit.com/oh-my-fish/oh-my-fish/e4f1c2e0219a17e2c748b824004c8d0b38055c16/docs/logo.svg" width="28px" height="28px"/> [Oh My Fish][oh-my-fish] in-offical package repository

For the official documentation, see their [README][official-readme].

## Usage

```shell script
$ omf repositories add https://gitlab.com/ohmyfish/pkgs
```

[oh-my-fish]: https://github.com/oh-my-fish/oh-my-fish
[official-readme]: https://github.com/oh-my-fish/packages-main/blob/master/README.md
